from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = [
        "username",
        "email",
        "apodo",
        "arte",
        "is_staff"
    ]
    fieldsets = UserAdmin.fieldsets + ((None, {"fields": ("apodo","arte",)}),)
    add_fieldsets = UserAdmin.add_fieldsets + ((None, {"fields": ("apodo","arte")}),)

admin.site.register(CustomUser,CustomUserAdmin)
