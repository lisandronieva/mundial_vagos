from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
    apodo = models.TextField(null=True,blank=True)
    arte = models.URLField(null=True,blank=True)