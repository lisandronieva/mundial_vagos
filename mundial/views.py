from django.shortcuts import render, redirect
from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView
from .models import Apuesta, Equipo
from accounts.models import CustomUser
from django.urls import reverse_lazy
# Create your views here.
from .models import Partido
import table_calc
import operator
from datetime import datetime, timedelta
from .ajuste_tiempo import SHIFT_TIME


GRUPOS_LETRAS = ["A","B","C","D","E","F","G","H"]
GRUPOS_ALTOS = ["I","J","K","L","M"]
REALIDAD = "realidad"
PUNTAJE_EXACTO = {"GRUPO":3,"I":5,"J":6,"K":7,"L":7,"M":8}
PUNTAJE_GANADOR = {"GRUPO":1,"I":2,"J":3,"K":4,"L":4,"M":5}
PUNTAJE_PUNTERO = 5
PUNTAJE_SEGUNDO = 3
PUNTAJE_COINCIDENCIA = 1
CUP_ICON="&#127942"


class MundialHomeView(TemplateView):
    template_name = "home.html"

class CompletadoView(TemplateView):
    template_name = "completado.html"

class ApuestaGrupoView(TemplateView):
    template_name = "apuesta.html"

    def get_context_data(self, **kwargs):
        context = super(ApuestaGrupoView, self).get_context_data(**kwargs)
        vieja_letra = self.get_last_apuesta()
        grupo_letra = str(chr(ord(vieja_letra[0])+1))
        partidos_grupo = self.extract_normalized_partidos(grupo_letra)
        equipos = Equipo.objects.all()
        context.update({'partidos_grupo': partidos_grupo,'paises':equipos,'grupo':grupo_letra,'vieja_letra':vieja_letra})
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        req_post = self.request.POST
        letra = context["grupo"] 
        contar_lista = [x for x in req_post.keys() if x[0] == "G"]
        iterator = int((len(contar_lista) )/2)
        partidos_reales = self.extract_normalized_partidos(letra)
        for index in range(1,iterator+1):
            fase_partido = "G"+letra+"P"+str(index)
            #partido_real = Partido.objects.get(fase=fase_partido)
            partidos_reales_filtrados = [item for item in partidos_reales if (item["fase"] == fase_partido)]
            partido_real = partidos_reales_filtrados[0]
            partido_objeto = Partido.objects.get(fase=partido_real["fase"])
            equipo_1 = Equipo.objects.get(nombre=partido_real['equipo_1']['nombre'])
            equipo_2 = Equipo.objects.get(nombre=partido_real['equipo_2']['nombre'])
            gol_1 = int(req_post[fase_partido+"E1"])
            gol_2 = int(req_post[fase_partido+"E2"])
            if gol_1 > gol_2:
                ganador = equipo_1
            elif gol_2 > gol_1:
                ganador = equipo_2
            else:
                if "P"+fase_partido in req_post.keys():
                    ganador_penales = int(req_post["P"+fase_partido])
                    if ganador_penales == 1:
                        ganador = equipo_1
                    else:
                        ganador = equipo_2
                else:
                    ganador = equipo_1
            apuesta_real = Apuesta(
                partido=partido_objeto,
                apostador=self.request.user,
                gol_1=gol_1,
                gol_2=gol_2,
                ganador=ganador,
                equipo_1=equipo_1,
                equipo_2=equipo_2
                )
            apuesta_real.save()
        letra_nueva = str(chr(ord(letra[0])+1))
        if ord(letra_nueva) > ord("M"):
            return redirect("completado")
        partidos_grupo = self.extract_normalized_partidos(letra_nueva)
        equipos = Equipo.objects.all()
        context.update({'partidos_grupo': partidos_grupo,'paises':equipos,'grupo':letra_nueva})
        return self.render_to_response(context)
   
    def get_last_apuesta(self):
        interador_apuestas = Apuesta.objects.filter(apostador=self.request.user)
        letra = ord("A")-1
        for apuesta_mirar in interador_apuestas:
            fase_mirar = apuesta_mirar.partido.fase
            letra_mirar = ord(fase_mirar[1])
            if letra_mirar > letra:
                letra = letra_mirar
        return chr(letra)

    def extract_normalized_partidos(self, grupo_letra):
        temp_partidos = Partido.objects.filter(fase__startswith='G'+grupo_letra)
        norm_partidos = []
        for partido in temp_partidos:
            if grupo_letra in GRUPOS_LETRAS:
                equipo_1 = {"nombre":partido.equipo_1.nombre,"codigo_bandera":partido.equipo_1.codigo_bandera}
                equipo_2 = {"nombre":partido.equipo_2.nombre,"codigo_bandera":partido.equipo_2.codigo_bandera}
                fecha = partido.fecha
                fase = partido.fase
                t_partido = {"equipo_1":equipo_1,"equipo_2":equipo_2,"fecha":fecha,"fase":fase}
                norm_partidos.append(t_partido)
            elif grupo_letra == "I":
                letra_primero = partido.equipo_1.nombre[-1] 
                letra_segundo = partido.equipo_2.nombre[-1]
                primero = self.calculate_tabla(0,letra_primero)
                segundo = self.calculate_tabla(1,letra_segundo)
                fecha = partido.fecha
                fase = partido.fase
                t_partido = {"equipo_1":primero,"equipo_2":segundo,"fecha":fecha,"fase":fase}
                norm_partidos.append(t_partido)
            elif grupo_letra in ["J","K","M"]:
                equipo_1 = self.calculate_ganador(partido.equipo_1.nombre[-4:])
                equipo_2 = self.calculate_ganador(partido.equipo_2.nombre[-4:])
                fecha = partido.fecha
                fase = partido.fase
                t_partido = {"equipo_1":equipo_1,"equipo_2":equipo_2,"fecha":fecha,"fase":fase}
                norm_partidos.append(t_partido)
            elif grupo_letra == "L":
                equipo_1 = self.calculate_perdedor(partido.equipo_1.nombre[-4:])
                equipo_2 = self.calculate_perdedor(partido.equipo_2.nombre[-4:])
                fecha = partido.fecha
                fase = partido.fase
                t_partido = {"equipo_1":equipo_1,"equipo_2":equipo_2,"fecha":fecha,"fase":fase}
                norm_partidos.append(t_partido)


        return norm_partidos

    def calculate_tabla(self,ordinal,grupo_letra):
        #temp_partidos = Partido.objects.filter(fase__startswith='G'+grupo_letra)
        temp_apuestas = Apuesta.objects.filter(partido__fase__startswith='G'+grupo_letra).filter(apostador=self.request.user)
        tabla_secuencia = []
        for apues in temp_apuestas:
            equipo_1 = apues.equipo_1.pk
            equipo_2 = apues.equipo_2.pk
            gol_1 = apues.gol_1
            gol_2 = apues.gol_2
            resultado = {"equipo0":str(equipo_1),"equipo1":str(equipo_2),"gol0":gol_1,"gol1":gol_2,"emp_ganador":1}
            tabla_secuencia.append(resultado)
        lista_final = table_calc.tabla(tabla_secuencia)
        pk_equipo_ordinal = lista_final[ordinal]["equipo"]
        equipo_sel = Equipo.objects.get(pk=pk_equipo_ordinal)
        equipo_nec = {"nombre":equipo_sel.nombre,"codigo_bandera":equipo_sel.codigo_bandera}
        return equipo_nec
        
    def calculate_ganador(self,partido_fase):
        partido_fase_selected = Partido.objects.get(fase=partido_fase)
        mis_partidos = Apuesta.objects.filter(apostador=self.request.user)
        partido_gan = mis_partidos.get(partido=partido_fase_selected)
        
        ganador = {"nombre":partido_gan.ganador.nombre,"codigo_bandera":partido_gan.ganador.codigo_bandera}
        return ganador

    def calculate_perdedor(self,partido_fase):
        partido_fase_selected = Partido.objects.get(fase=partido_fase)
        mis_partidos = Apuesta.objects.filter(apostador=self.request.user)
        partido_gan = mis_partidos.get(partido=partido_fase_selected)
        if partido_gan.ganador.nombre == partido_gan.equipo_1.nombre:
            perdedor = partido_gan.equipo_2
        else:
            perdedor = partido_gan.equipo_1
        perdedor_ext = {"nombre":perdedor.nombre,"codigo_bandera":perdedor.codigo_bandera}
        return perdedor_ext

class TablaPosicionesView(TemplateView):
    template_name = "tabla_posiciones.html"

    def get_context_data(self, **kwargs):
        context = super(TablaPosicionesView, self).get_context_data(**kwargs)
        apostadores = CustomUser.objects.exclude(username=REALIDAD).exclude(username="mandramas")
        apos_list = []
        for apostador in apostadores:
            apos_dict = {"pk":apostador.pk,"username":apostador.username,"apodo":apostador.apodo}
            apos_dict["puntaje_total"] = self.calcular_puntaje_total(apostador)
            apos_list.append(apos_dict)
        apos_list.sort(key=operator.itemgetter('puntaje_total'), reverse=True)
        context.update({"apostadores":apos_list})
        return context

    def calcular_puntaje_total(self,apostador):
        puntaje = 0
        realidad = CustomUser.objects.get(username=REALIDAD)
        apuestas = Apuesta.objects.filter(apostador=apostador)
        for ap in apuestas:
            fase = ap.partido.fase
            ap_real_l = Apuesta.objects.filter(apostador=realidad).filter(partido__fase=fase)
            if len(ap_real_l) == 1:
                ap_real = ap_real_l[0]
                puntaje = puntaje + self.calcular_puntaje_tab(ap,ap_real)
        for letra in GRUPOS_LETRAS:
            serie_real = Apuesta.objects.filter(apostador=realidad).filter(partido__fase__startswith="G"+letra)
            if len(serie_real) == 6: 
                puntaje = puntaje + self.calcular_puntaje_clasificados_tab(letra,apostador,realidad)
        return puntaje

    def calcular_puntaje_tab(self,apuesta_personal,resultado):
        puntaje = 0
        fase = apuesta_personal.partido.fase
        letra = fase[1]
        if letra in GRUPOS_LETRAS:
            if (apuesta_personal.gol_1 == resultado.gol_1) and (apuesta_personal.gol_2 == resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_EXACTO['GRUPO']
            elif (apuesta_personal.gol_1 > apuesta_personal.gol_2) and (resultado.gol_1 > resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_GANADOR['GRUPO']
            elif (apuesta_personal.gol_1 < apuesta_personal.gol_2) and (resultado.gol_1 < resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_GANADOR['GRUPO']
            elif (apuesta_personal.gol_1 == apuesta_personal.gol_2) and (resultado.gol_1 == resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_GANADOR['GRUPO']
        else:
            if (apuesta_personal.gol_1 == resultado.gol_1) and (apuesta_personal.gol_2 == resultado.gol_2 ) and (apuesta_personal.ganador == resultado.ganador):
                puntaje = puntaje + PUNTAJE_EXACTO[letra]
            elif (apuesta_personal.ganador == resultado.ganador):
                puntaje = puntaje + PUNTAJE_GANADOR[letra]
            if (apuesta_personal.equipo_1 == resultado.equipo_1) or (apuesta_personal.equipo_1 == resultado.equipo_2):
                puntaje = puntaje + PUNTAJE_COINCIDENCIA
            if (apuesta_personal.equipo_2 == resultado.equipo_1) or (apuesta_personal.equipo_2 == resultado.equipo_2):
                puntaje = puntaje + PUNTAJE_COINCIDENCIA
        return puntaje
    
    def calcular_puntaje_clasificados_tab(self,letra,autor,realidad):
        puntaje = 0
        clasificado_primer = self.calculate_tabla_tab(0,letra,autor)
        clasificado_segundo = self.calculate_tabla_tab(1,letra,autor)
        clasificado_primer_real = self.calculate_tabla_tab(0,letra,realidad)
        clasificado_segundo_real = self.calculate_tabla_tab(1,letra,realidad)
        if clasificado_primer['nombre'] == clasificado_primer_real['nombre']:
            puntaje = puntaje + int(PUNTAJE_PUNTERO)
        if clasificado_segundo['nombre'] == clasificado_segundo_real['nombre']:
            puntaje = puntaje + int(PUNTAJE_SEGUNDO)
        return puntaje

    def calculate_tabla_tab(self,ordinal,grupo_letra,autor): #Ordinal es 0 para 1ero, 1 para 2do
        #temp_partidos = Partido.objects.filter(fase__startswith='G'+grupo_letra)
        temp_apuestas = Apuesta.objects.filter(partido__fase__startswith='G'+grupo_letra).filter(apostador=autor)
        tabla_secuencia = []
        for apues in temp_apuestas:
            equipo_1 = apues.equipo_1.pk
            equipo_2 = apues.equipo_2.pk
            gol_1 = apues.gol_1
            gol_2 = apues.gol_2
            resultado = {"equipo0":str(equipo_1),"equipo1":str(equipo_2),"gol0":gol_1,"gol1":gol_2,"emp_ganador":1}
            tabla_secuencia.append(resultado)
        lista_final = table_calc.tabla(tabla_secuencia)
        pk_equipo_ordinal = lista_final[ordinal]["equipo"]
        equipo_sel = Equipo.objects.get(pk=pk_equipo_ordinal)
        equipo_nec = {"nombre":equipo_sel.nombre,"codigo_bandera":equipo_sel.codigo_bandera}
        return equipo_nec

class DetallesApuestaView(DetailView):
    template_name = "detalles_apuesta.html"
    model = CustomUser

    def get_context_data(self, **kwargs):
        context = super(DetallesApuestaView, self).get_context_data(**kwargs)
        apostador_pk = self.kwargs['pk']
        autor = CustomUser.objects.get(pk=apostador_pk)
        content_apuestas = Apuesta.objects.filter(apostador=autor)
        realidad = CustomUser.objects.get(username=REALIDAD)
        mis_partidos = self.modelar_mis_partidos(content_apuestas,realidad,autor)
        context.update({"mis_partidos":mis_partidos})
        return context

    def modelar_mis_partidos(self,las_apuestas,realidad,autor):
        mis_partidos = {}
        for ap in las_apuestas:
            fase = ap.partido.fase
            letra = fase[1]
            if letra in GRUPOS_LETRAS:
                partido = {"equipo_1_nombre":ap.equipo_1.nombre,"equipo_1_flag":ap.equipo_1.codigo_bandera,"equipo_2_nombre":ap.equipo_2.nombre,"equipo_2_flag":ap.equipo_2.codigo_bandera}
                partido['gol_1'] = ap.gol_1
                partido['gol_2'] = ap.gol_2
                mis_partidos[letra] = mis_partidos.get(letra,{})
                mis_partidos[letra]['titulo'] = f"Grupo {letra}"
                partido_real = Apuesta.objects.filter(apostador=realidad).filter(partido__fase=fase)
                if len(partido_real) == 1:
                    partido["equipo_1_nombre_real"] = partido_real[0].equipo_1.nombre
                    partido["equipo_2_nombre_real"] = partido_real[0].equipo_2.nombre
                    partido["equipo_1_flag_real"] = partido_real[0].equipo_1.codigo_bandera
                    partido["equipo_2_flag_real"] = partido_real[0].equipo_2.codigo_bandera
                    partido['gol_1_real'] = partido_real[0].gol_1
                    partido['gol_2_real'] = partido_real[0].gol_2
                    if partido_real[0].ganador == partido_real[0].equipo_1:
                        partido['ganador_real'] = 1
                    else:
                        partido['ganador_real'] = 2
                    partido['puntaje'] = self.calcular_puntaje(ap,partido_real[0])
                else:
                    partido["equipo_1_nombre_real"] = ap.equipo_1.nombre
                    partido["equipo_2_nombre_real"] = ap.equipo_2.nombre
                    partido["equipo_1_flag_real"] = "XX"
                    partido["equipo_2_flag_real"] = "XX"
                    partido['gol_1_real'] = "?"
                    partido['gol_2_real'] = "?"
                    partido['puntaje'] = "?"
                proto = mis_partidos[letra].get('partidos', [])
                proto.append(partido)
                mis_partidos[letra]['partidos'] = proto
            elif letra in GRUPOS_ALTOS:
                partido = {"equipo_1_nombre":ap.equipo_1.nombre,"equipo_1_flag":ap.equipo_1.codigo_bandera,"equipo_2_nombre":ap.equipo_2.nombre,"equipo_2_flag":ap.equipo_2.codigo_bandera}
                partido['gol_1'] = ap.gol_1
                partido['gol_2'] = ap.gol_2
                if ap.gol_1 == ap.gol_2:
                    if ap.ganador == ap.equipo_1:
                        partido['ganador_1'] = CUP_ICON#"&#127942"
                    else:
                        partido['ganador_2'] =CUP_ICON
                mis_partidos[letra] = mis_partidos.get(letra,{})
                if letra == "I":
                    mis_partidos[letra]['titulo'] = f"Octavos de final"
                elif letra == "J":
                    mis_partidos[letra]['titulo'] = f"Cuartos de final"
                elif letra == "K":
                    mis_partidos[letra]['titulo'] = f"Semifinales"
                elif letra == "L":
                    mis_partidos[letra]['titulo'] = f"Partido tercer puesto"
                elif letra == "M":
                    mis_partidos[letra]['titulo'] = f"Final"
                partido_real = Apuesta.objects.filter(apostador=realidad).filter(partido__fase=fase)
                if len(partido_real) == 1:
                    partido["equipo_1_nombre_real"] = partido_real[0].equipo_1.nombre
                    partido["equipo_2_nombre_real"] = partido_real[0].equipo_2.nombre
                    partido["equipo_1_flag_real"] = partido_real[0].equipo_1.codigo_bandera
                    partido["equipo_2_flag_real"] = partido_real[0].equipo_2.codigo_bandera
                    partido['gol_1_real'] = partido_real[0].gol_1
                    partido['gol_2_real'] = partido_real[0].gol_2
                    if partido_real[0].gol_1 == partido_real[0].gol_2:
                        if partido_real[0].ganador == partido_real[0].equipo_1:
                            partido['ganador_real_1'] = CUP_ICON
                        else:
                            partido['ganador_real_2'] = CUP_ICON
                    partido['puntaje'] = self.calcular_puntaje(ap,partido_real[0])
                else:
                    partido["equipo_1_nombre_real"] = self.calcular_clasificado_real(fase=fase,equipo=1)["nombre"]
                    partido["equipo_2_nombre_real"] = self.calcular_clasificado_real(fase=fase,equipo=2)["nombre"]
                    partido["equipo_1_flag_real"] = self.calcular_clasificado_real(fase=fase,equipo=1)["codigo_bandera"]
                    partido["equipo_2_flag_real"] = self.calcular_clasificado_real(fase=fase,equipo=2)["codigo_bandera"]
                    if partido["equipo_1_flag_real"] == "cl":
                        partido["equipo_1_flag_real"] = "xx"
                    if partido["equipo_2_flag_real"] == "cl":
                        partido["equipo_2_flag_real"] = "xx"
                    partido['gol_1_real'] = "?"
                    partido['gol_2_real'] = "?"
                    partido['puntaje'] = "?"
                proto = mis_partidos[letra].get('partidos', [])
                proto.append(partido)
                mis_partidos[letra]['partidos'] = proto
        for letra in GRUPOS_LETRAS:
            serie_real = Apuesta.objects.filter(apostador=realidad).filter(partido__fase__startswith="G"+letra)
            clasificados = {
                "primero":self.calculate_tabla_del(0,letra,autor),
                "segundo":self.calculate_tabla_del(1,letra,autor),
                "primero_real":{"nombre":"???","codigo_bandera":"xx"},
                "segundo_real":{"nombre":"???","codigo_bandera":"xx"},
            }
            if len(serie_real) == 6: 
                clasificados["primero_real"] = self.calculate_tabla_del(0,letra,realidad)
                clasificados["segundo_real"] = self.calculate_tabla_del(1,letra,realidad)
                clasificados["puntaje"] = self.calcular_puntaje_clasificados(letra,autor,realidad)
            mis_partidos[letra]["clasificados"] = clasificados
        return mis_partidos

    def calcular_puntaje(self,apuesta_personal,resultado):
        puntaje = 0
        fase = apuesta_personal.partido.fase
        letra = fase[1]
        if letra in GRUPOS_LETRAS:
            if (apuesta_personal.gol_1 == resultado.gol_1) and (apuesta_personal.gol_2 == resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_EXACTO['GRUPO']
            elif (apuesta_personal.gol_1 > apuesta_personal.gol_2) and (resultado.gol_1 > resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_GANADOR['GRUPO']
            elif (apuesta_personal.gol_1 < apuesta_personal.gol_2) and (resultado.gol_1 < resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_GANADOR['GRUPO']
            elif (apuesta_personal.gol_1 == apuesta_personal.gol_2) and (resultado.gol_1 == resultado.gol_2 ):
                puntaje = puntaje + PUNTAJE_GANADOR['GRUPO']
        else:
            if (apuesta_personal.gol_1 == resultado.gol_1) and (apuesta_personal.gol_2 == resultado.gol_2 ) and (apuesta_personal.ganador == resultado.ganador):
                puntaje = puntaje + PUNTAJE_EXACTO[letra]
            elif (apuesta_personal.ganador == resultado.ganador):
                puntaje = puntaje + PUNTAJE_GANADOR[letra]
            if (apuesta_personal.equipo_1 == resultado.equipo_1) or (apuesta_personal.equipo_1 == resultado.equipo_2):
                puntaje = puntaje + PUNTAJE_COINCIDENCIA
            if (apuesta_personal.equipo_2 == resultado.equipo_1) or (apuesta_personal.equipo_2 == resultado.equipo_2):
                puntaje = puntaje + PUNTAJE_COINCIDENCIA
        return puntaje

    def calculate_tabla_del(self,ordinal,grupo_letra,autor): #Ordinal es 0 para 1ero, 1 para 2do
        #temp_partidos = Partido.objects.filter(fase__startswith='G'+grupo_letra)
        temp_apuestas = Apuesta.objects.filter(partido__fase__startswith='G'+grupo_letra).filter(apostador=autor)
        tabla_secuencia = []
        for apues in temp_apuestas:
            equipo_1 = apues.equipo_1.pk
            equipo_2 = apues.equipo_2.pk
            gol_1 = apues.gol_1
            gol_2 = apues.gol_2
            resultado = {"equipo0":str(equipo_1),"equipo1":str(equipo_2),"gol0":gol_1,"gol1":gol_2,"emp_ganador":1}
            tabla_secuencia.append(resultado)
        lista_final = table_calc.tabla(tabla_secuencia)
        pk_equipo_ordinal = lista_final[ordinal]["equipo"]
        equipo_sel = Equipo.objects.get(pk=pk_equipo_ordinal)
        equipo_nec = {"nombre":equipo_sel.nombre,"codigo_bandera":equipo_sel.codigo_bandera}
        if equipo_nec["codigo_bandera"] == "cl":
            equipo_nec["codigo_bandera"] = "xx"
        return equipo_nec

    def calcular_puntaje_clasificados(self,letra,autor,realidad):
        puntaje = 0
        clasificado_primer = self.calculate_tabla_del(0,letra,autor)
        clasificado_segundo = self.calculate_tabla_del(1,letra,autor)
        clasificado_primer_real = self.calculate_tabla_del(0,letra,realidad)
        clasificado_segundo_real = self.calculate_tabla_del(1,letra,realidad)
        if clasificado_primer['nombre'] == clasificado_primer_real['nombre']:
            puntaje = puntaje + int(PUNTAJE_PUNTERO)
        if clasificado_segundo['nombre'] == clasificado_segundo_real['nombre']:
            puntaje = puntaje + int(PUNTAJE_SEGUNDO)
        return puntaje

    def calcular_clasificado_real(self,fase,equipo):
        realidad = CustomUser.objects.get(username=REALIDAD)
        resultado =  {"nombre":"...","codigo_bandera":"XX"}
        letra = fase[1]
        nombre_equipo = "..."
        flag = "XX"
        partido = Partido.objects.get(fase=fase)
        if partido:
            if equipo == 1:
                nombre_equipo = partido.equipo_1.nombre
                flag = partido.equipo_1.codigo_bandera
                    
            elif equipo == 2:
                nombre_equipo = partido.equipo_2.nombre
                flag = partido.equipo_2.codigo_bandera
        if letra in GRUPOS_LETRAS:
            resultado["nombre"] = nombre_equipo
            resultado["codigo_bandera"] = flag
        elif letra == "I":
            if nombre_equipo.startswith("Primero"):
                grupo = nombre_equipo[-1]
                resultado = self.calculate_tabla_del(0,grupo,realidad)
            elif nombre_equipo.startswith("Segundo"):
                grupo = nombre_equipo[-1]
                resultado = self.calculate_tabla_del(1,grupo,realidad)
        elif letra in ["J","K","M","L"]:
            partido_anterior = nombre_equipo[-4:]
            apuesta_anterior = Apuesta.objects.filter(apostador=realidad).filter(partido__fase=partido_anterior)
            if apuesta_anterior:
                if nombre_equipo.startswith("Ganador"):
                    if len(apuesta_anterior) == 1:
                        if apuesta_anterior[0].ganador:
                            nombre_equipo = apuesta_anterior[0].ganador.nombre
                            flag = apuesta_anterior[0].ganador.codigo_bandera
                elif nombre_equipo.startswith("Perdedor"):
                    if apuesta_anterior[0].ganador:
                        ganador_pk = apuesta_anterior[0].ganador.pk
                        equipo_1_pk = apuesta_anterior[0].equipo_1.pk
                        if ganador_pk == equipo_1_pk:
                            nombre_equipo = apuesta_anterior[0].equipo_2.nombre
                            flag = apuesta_anterior[0].equipo_2.codigo_bandera
                        else:
                            nombre_equipo = apuesta_anterior[0].equipo_1.nombre
                            flag = apuesta_anterior[0].equipo_1.codigo_bandera
            resultado["nombre"] = nombre_equipo
            resultado["codigo_bandera"] = flag
        if resultado["codigo_bandera"] == "cl":
            resultado["codigo_bandera"] == "xx"
        return resultado 


class BackdoorList(ListView):
    template_name = "backdoor_lista.html"
    paginate_by = 100

    def get_queryset(self):
        queryset = Apuesta.objects.filter(apostador=self.request.user)
        return queryset

class BackdoorEdit(UpdateView):
    template_name = "backdoor_edit.html"
    model = Apuesta
    fields = (
        "partido",
        "equipo_1",
        "equipo_2",
        "apostador",
        "gol_1",
        "gol_2",
        "ganador"
        )
    success_url = reverse_lazy("backdoor")

class NextPartidoView(TemplateView):
    template_name = "next_partido.html"

    def get_context_data(self, **kwargs):
        context = super(NextPartidoView, self).get_context_data(**kwargs)
        referencia = datetime.now()+ timedelta(hours=SHIFT_TIME)
        nextPartidos = Partido.objects.filter(fecha__gte=referencia).order_by('fecha')
        nextPartidos_list = []
        np_dic = {"partido":nextPartidos[0]}
        nextPartidos_list.append(np_dic)
        for simultaneo in nextPartidos:
            if (nextPartidos_list[0]["partido"].fecha == simultaneo.fecha) and not(nextPartidos_list[0]["partido"].fase == simultaneo.fase):
                nextPartidos_list.append({"partido":simultaneo})
#        if nextPartidos[1].fecha == nextPartidos[0].fecha:
#            if nextPartidos[1].fase != 
#            nextPartidos_list.append({"partido":nextPartidos[1]})
        for partido in nextPartidos_list:
            fase = partido["partido"].fase
            letra = fase[1]
            fecha = partido["partido"].fecha
            if letra in GRUPOS_ALTOS:
                arreglo_partido = {
                    "equipo_1":calcular_clasificado_real_ext(fase,1),
                    "equipo_2":calcular_clasificado_real_ext(fase,2),
                    "fase": fase,
                    "fecha": fecha
                }
                partido['partido'] = arreglo_partido
            partido['apuestas'] = Apuesta.objects.filter(partido__fase=fase).exclude(apostador__username=REALIDAD).exclude(apostador__username="mandramas")
            partido['fecha_ar'] = fecha + timedelta(hours=-4)
            partido_real = Apuesta.objects.filter(partido__fase=fase).filter(apostador__username=REALIDAD)
            if len(partido_real) == 1:
                partido['realidad'] = partido_real[0]
        context.update({"next_partidos":nextPartidos_list,"referencia":referencia,"referencia_cierre":referencia+timedelta(hours=-2)})
        return context


def calcular_clasificado_real_ext(fase,equipo):
    realidad = CustomUser.objects.get(username=REALIDAD)
    resultado =  {"nombre":"...","codigo_bandera":"XX"}
    letra = fase[1]
    nombre_equipo = "..."
    flag = "XX"
    partido = Partido.objects.get(fase=fase)
    if partido:
        if equipo == 1:
            nombre_equipo = partido.equipo_1.nombre
            flag = partido.equipo_1.codigo_bandera
        elif equipo == 2:
            nombre_equipo = partido.equipo_2.nombre
            flag = partido.equipo_2.codigo_bandera
  
    if letra in GRUPOS_LETRAS:
        resultado["nombre"] = nombre_equipo
        resultado["codigo_bandera"] = flag
    elif letra == "I":
        if nombre_equipo.startswith("Primero"):
            grupo = nombre_equipo[-1]
            resultado = calculate_tabla_ext(0,grupo,realidad)
        elif nombre_equipo.startswith("Segundo"):
            grupo = nombre_equipo[-1]
            resultado = calculate_tabla_ext(1,grupo,realidad)
    elif letra in ["J","K","M","L"]:
        partido_anterior = nombre_equipo[-4:]
        apuesta_anterior = Apuesta.objects.filter(apostador=realidad).filter(partido__fase=partido_anterior)
        if apuesta_anterior:
            if nombre_equipo.startswith("Ganador"):
                if len(apuesta_anterior) == 1:
                    nombre_equipo = apuesta_anterior[0].ganador.nombre
                    flag = apuesta_anterior[0].ganador.codigo_bandera
            elif nombre_equipo.startswith("Perdedor"):
                ganador_pk = apuesta_anterior[0].ganador.pk
                equipo_1_pk = apuesta_anterior[0].equipo_1.pk
                if ganador_pk == equipo_1_pk:
                    nombre_equipo = apuesta_anterior[0].equipo_2.nombre
                    flag = apuesta_anterior[0].equipo_2.codigo_bandera
                else:
                    nombre_equipo = apuesta_anterior[0].equipo_1.nombre
                    flag = apuesta_anterior[0].equipo_1.codigo_bandera
        resultado["nombre"] = nombre_equipo
        resultado["codigo_bandera"] = flag
    return resultado 

def calculate_tabla_ext(ordinal,grupo_letra,autor): #Ordinal es 0 para 1ero, 1 para 2do
    #temp_partidos = Partido.objects.filter(fase__startswith='G'+grupo_letra)
    temp_apuestas = Apuesta.objects.filter(partido__fase__startswith='G'+grupo_letra).filter(apostador=autor)
    tabla_secuencia = []
    for apues in temp_apuestas:
        equipo_1 = apues.equipo_1.pk
        equipo_2 = apues.equipo_2.pk
        gol_1 = apues.gol_1
        gol_2 = apues.gol_2
        resultado = {"equipo0":str(equipo_1),"equipo1":str(equipo_2),"gol0":gol_1,"gol1":gol_2,"emp_ganador":1}
        tabla_secuencia.append(resultado)
    lista_final = table_calc.tabla(tabla_secuencia)
    pk_equipo_ordinal = lista_final[ordinal]["equipo"]
    equipo_sel = Equipo.objects.get(pk=pk_equipo_ordinal)
    equipo_nec = {"nombre":equipo_sel.nombre,"codigo_bandera":equipo_sel.codigo_bandera}
    return equipo_nec