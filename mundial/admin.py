from django.contrib import admin
from .models import Equipo, Partido, Apuesta
# Register your models here.

admin.site.register(Equipo)
admin.site.register(Partido)
admin.site.register(Apuesta)