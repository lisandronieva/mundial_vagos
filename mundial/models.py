from django.db import models
from django.conf import settings

# Create your models here.
class Equipo(models.Model):
    nombre = models.TextField()
    codigo_bandera = models.CharField(max_length=10)

    def __str__(self):
        return self.nombre

class Partido(models.Model):
    equipo_1 = models.ForeignKey(Equipo, on_delete=models.CASCADE,related_name="Equipo_1",blank=True, null=True)
    equipo_2 = models.ForeignKey(Equipo, on_delete=models.CASCADE,related_name="Equipo_2",blank=True, null=True)
    fecha = models.DateTimeField()
    fase = models.CharField(max_length=10)

    def __str__(self):
        return self.fase

class Apuesta(models.Model):
    partido = models.ForeignKey(Partido,on_delete=models.CASCADE)
    apostador = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    gol_1 = models.PositiveSmallIntegerField()
    gol_2 = models.PositiveSmallIntegerField()
    ganador = models.ForeignKey(Equipo,on_delete=models.CASCADE,related_name="Equipo_ganador_apuesta",blank=True, null=True)
    equipo_1 = models.ForeignKey(Equipo, on_delete=models.CASCADE,related_name="Equipo_1_apuesta",blank=True, null=True)
    equipo_2 = models.ForeignKey(Equipo, on_delete=models.CASCADE,related_name="Equipo_2_apuesta",blank=True, null=True)