from django.urls import path
from .views import MundialHomeView,ApuestaGrupoView,CompletadoView,TablaPosicionesView,DetallesApuestaView,BackdoorList,BackdoorEdit,NextPartidoView

urlpatterns = [
    path("",MundialHomeView.as_view(), name="home"),
    path("apostar/",ApuestaGrupoView.as_view(), name="apuesta"),
    path("completado/",CompletadoView.as_view(), name="completado"),
    path("tabla",TablaPosicionesView.as_view(), name="tabla"),
    path("detalles/<int:pk>/", DetallesApuestaView.as_view(), name="detalles"),
    path("backdoor", BackdoorList.as_view(), name="backdoor"),
    path("backedit/<int:pk>/", BackdoorEdit.as_view(), name="backedit"),
    path("next_partido",NextPartidoView.as_view(), name="nextpartido"),    
]
