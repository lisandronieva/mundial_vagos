import operator

partidos = [
    {"equipo0":"URU","equipo1":"COR","gol0":0,"gol1":0,"emp_ganador":1},
    {"equipo0":"POR","equipo1":"GHA","gol0":3,"gol1":2,"emp_ganador":1},
    {"equipo0":"COR","equipo1":"GHA","gol0":2,"gol1":3,"emp_ganador":1},
    {"equipo0":"POR","equipo1":"URU","gol0":2,"gol1":0,"emp_ganador":1},
    {"equipo0":"GHA","equipo1":"URU","gol0":0,"gol1":2,"emp_ganador":1},
    {"equipo0":"COR","equipo1":"POR","gol0":2,"gol1":1,"emp_ganador":1},
]

def tabla(partidos):
    equipos = {}
    for p in partidos[0:2]: #en teoria los dos primeros partidos bastan
        equipos[p["equipo0"]] = {"puntos":0,"partidos_ganados":0,"partidos_jugados":0,"goles_favor":0,"diferencia_goles":0,"equipo":p["equipo0"]}
        equipos[p["equipo1"]] = {"puntos":0,"partidos_ganados":0,"partidos_jugados":0,"goles_favor":0,"diferencia_goles":0,"equipo":p["equipo1"]}
    for p in partidos:
        equipos[p["equipo0"]]["partidos_jugados"] =  equipos[p["equipo0"]]["partidos_jugados"] + 1
        equipos[p["equipo1"]]["partidos_jugados"] =  equipos[p["equipo1"]]["partidos_jugados"] + 1
        equipos[p["equipo0"]]["goles_favor"] =  equipos[p["equipo0"]]["goles_favor"] + p["gol0"]
        equipos[p["equipo1"]]["goles_favor"] =  equipos[p["equipo1"]]["goles_favor"] + p["gol1"]
        equipos[p["equipo0"]]["diferencia_goles"] =  equipos[p["equipo0"]]["diferencia_goles"] + p["gol0"] - p["gol1"]
        equipos[p["equipo1"]]["diferencia_goles"] =  equipos[p["equipo1"]]["diferencia_goles"] + p["gol1"] - p["gol0"]
        if p["gol0"] > p["gol1"]:
            equipos[p["equipo0"]]["partidos_ganados"] =  equipos[p["equipo0"]]["partidos_ganados"] + 1
            equipos[p["equipo0"]]["puntos"] =  equipos[p["equipo0"]]["puntos"] + 3
        elif p["gol1"] > p["gol0"]:
            equipos[p["equipo1"]]["partidos_ganados"] =  equipos[p["equipo1"]]["partidos_ganados"] + 1
            equipos[p["equipo1"]]["puntos"] =  equipos[p["equipo1"]]["puntos"] + 3
        else:
            equipos[p["equipo0"]]["puntos"] =  equipos[p["equipo0"]]["puntos"] + 1
            equipos[p["equipo1"]]["puntos"] =  equipos[p["equipo1"]]["puntos"] + 1
    lequipos = [equipos[x] for x in equipos.keys()]
    lequipos.sort(key=operator.itemgetter('goles_favor'), reverse=True)
    lequipos.sort(key=operator.itemgetter('diferencia_goles'), reverse=True)
    lequipos.sort(key=operator.itemgetter('partidos_ganados'), reverse=True)
    lequipos.sort(key=operator.itemgetter('puntos'), reverse=True)
    return lequipos

if __name__ == "__main__":
    eqp = tabla(partidos)
    for eq in eqp:
        print(f"Equipo:{eq['equipo']}      Puntos: {eq['puntos']}    partidos_ganados: {eq['partidos_ganados']}   diferencia_goles: {eq['diferencia_goles']}    goles_favor: {eq['goles_favor']}")